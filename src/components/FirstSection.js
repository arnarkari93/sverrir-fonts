import React, { PureComponent } from 'react';
import styled from 'styled-components';
import Rx from 'rxjs/Rx';

import { fontFamilies } from '../styles';

class FirstSection extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      fontFamily: fontFamilies.Typography,
      fontSize: '10em'
    };
  }

  componentDidMount() {
    this.createObservable([
      {
        word: 'A geometric typeface in three cuts based on Pitman Shorthand.',
        fontFamily: fontFamilies.Typography,
        fontSize: '10em'
      },
      {
        word: 'A geometric typeface in three cuts based on Pitman Shorthand.',
        fontFamily: fontFamilies.Iconography,
        fontSize: '9em'
      },
      {
        word: 'A geometric typeface in three cuts based on Pitman Shorthand.',
        fontFamily: fontFamilies.Cryptography,
        fontSize: '6em'
      }
    ]);
  }

  componentWillUnmount() {
    if (this.subscription) {
      this.subscription.unsubscribe && this.subscription.unsubscribe();
    }
  }

  createObservable(words) {
    const obs = Rx.Observable.zip(
      Rx.Observable.from(words),
      Rx.Observable.timer(1000, 3000),
      (value, timer) => value
    )
      .do(({ word, fontFamily, fontSize }) => {
        this.setState({ value: word, fontFamily, fontSize });
      })
      .repeat();
    this.subscription = obs.subscribe(x => {});
  }

  render() {
    const { fontFamily, fontSize, value } = this.state;
    return (
      <Container>
        <Word fontFamily={fontFamily} fontSize={fontSize}>
          {value}
        </Word>
      </Container>
    );
  }
}

const Word = styled.div`
  font-family: ${props => props.fontFamily};
  font-size: ${props => props.fontSize};
  word-wrap: break-word;
  height: 50rem;
`;

const Container = styled.div`
  display: flex;
  height: 100vh;
  margin: 0em 15%;
  justify-content: center;
  align-items: center;
`;

export default FirstSection;
