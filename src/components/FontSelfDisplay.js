import React, { PureComponent } from 'react';
import Rx from 'rxjs/Rx';
import styled from 'styled-components';

class FontSelfDisplay extends PureComponent {
  constructor(props) {
    super(props);
    this.state = { value: '' };
  }

  componentDidMount() {
    this.createObservable(this.props.typeOut);
  }

  componentWillUnmount() {
    if (this.subscription) {
      this.subscription.unsubscribe && this.subscription.unsubscribe();
    }
  }

  createObservable(sentence) {
    let obs = Rx.Observable.empty();
    if (this.props.repeatDelay) {
      obs = Rx.Observable.zip(
        Rx.Observable.from(sentence.split('')),
        Rx.Observable.timer(this.props.delay, this.props.typeSpeed),
        (value, timer) => value
      )
        .do(x => {
          this.setState(prevState => ({ value: prevState.value + x }));
        })
        .delay(this.props.repeatDelay)
        .do(() => this.setState({ value: '' }))
        .repeat();
    } else {
      obs = Rx.Observable.zip(
        Rx.Observable.from(sentence.split('')),
        Rx.Observable.timer(this.props.delay, this.props.typeSpeed),
        (value, timer) => value
      ).do(x => {
        this.setState(prevState => ({ value: prevState.value + x }));
      });
    }

    this.subscription = obs.subscribe(x => {});
  }

  render() {
    const { fontFamily, height, color } = this.props;
    return (
      <Container fontFamily={fontFamily} height={height} color={color}>
        {this.state.value}
      </Container>
    );
  }
}

const Container = styled.div`
  font-family: ${props => props.fontFamily};
  color: ${props => (props.color ? props.color : '#000')};
  font-size: 13em;
  margin: 0.2em 0em;
  line-height: ${props => props.height}px;
  word-wrap: break-word;

  ::selection {
    color: #00f;
    background-color: none;
  }
`;

export default FontSelfDisplay;
