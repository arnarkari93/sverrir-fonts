import React, { PureComponent } from 'react';
import styled from 'styled-components';

import Header from './Header';
import FontSelfDisplay from './FontSelfDisplay';
import ColorSection from './ColorSection';
import SecondSection from './SecondSection';
import FontTest from './FontTest';
import ClickSection from './ClickSection';
import About from './About';
import { fontFamilies } from '../styles';

const LandingContainer = styled.div`
  margin: 8em 3em 0em 3em;
  min-height: 100vh;
`;

const Container = styled.div``;

const Fart = styled.div`
  position: relative;
`;

const Shit = styled.div`
  position: absolute;
  top: 0;
`;

const renderLanding = () => {
  return (
    <LandingContainer>
      <FontSelfDisplay
        typeOut={'Typography'}
        fontFamily={fontFamilies.Typography}
        height={140}
        delay={800} // 800
        typeSpeed={120}
      />
      <FontSelfDisplay
        typeOut={'Stenography'}
        fontFamily={fontFamilies.Iconography}
        height={140}
        delay={2120} // 2120
        typeSpeed={120}
      />
      <Fart>
        <FontSelfDisplay
          typeOut={'Cryptography'}
          fontFamily={fontFamilies.Cryptography}
          height={180}
          delay={3320} // 3320
          typeSpeed={120}
        />
        <Shit>
          <FontSelfDisplay
            typeOut={'Cryptography'}
            fontFamily={fontFamilies.Typography}
            height={100}
            delay={3320} // 3320
            typeSpeed={120}
            color={'#00f'}
          />
        </Shit>
      </Fart>
    </LandingContainer>
  );
};

class FontApp extends PureComponent {
  render() {
    return (
      <Container>
        <Header />
        {renderLanding()}
        <ColorSection />
        <SecondSection />
        <FontTest />
        <About />
        <ClickSection />
      </Container>
    );
  }
}

export default FontApp;
