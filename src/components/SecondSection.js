import React, { PureComponent } from 'react';
import styled from 'styled-components';
import Rx from 'rxjs/Rx';

import { fontFamilies } from '../styles';

class SecondSection extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      fontFamily: fontFamilies.Typography,
      fontSize: '10em',
      color: '#000'
    };
  }

  componentDidMount() {
    this.createObservable(WORDS);
  }

  componentWillUnmount() {
    if (this.subscription) {
      this.subscription.unsubscribe && this.subscription.unsubscribe();
    }
  }

  chooseColor = () => {
    if (Math.random() > 0.6) {
      const rand = Math.random();
      if (rand < 0.5) {
        return '#0000ff';
      } else {
        return '#00ff00';
      }
    }

    return '#000';
  };

  createObservable(words) {
    const obs = Rx.Observable.zip(
      Rx.Observable.from(words),
      Rx.Observable.timer(1000, 600),
      (value, timer) => value
    )
      .do(({ word, fontFamily }) => {
        this.setState({
          value: word,
          fontFamily,
          fontSize: '13em',
          color: this.chooseColor()
        });
      })
      .repeat();
    this.subscription = obs.subscribe(x => {});
  }

  render() {
    const { fontFamily, fontSize, value, color } = this.state;
    return (
      <Container>
        <Word fontFamily={fontFamily} fontSize={fontSize} color={color}>
          {value}
        </Word>
      </Container>
    );
  }
}

const Word = styled.div`
  font-family: ${props => props.fontFamily};
  font-size: ${props => props.fontSize};
  color: ${props => props.color};
  word-wrap: break-word;
  height: 50rem;
  display: flex;
  align-items: center;
  ::selection {
    color: #00f;
    background-color: none;
  }
`;

const Container = styled.div`
  display: flex;
  height: 100vh;
  margin: 0em 15%;
  justify-content: center;
  align-items: center;
  align-content: center;
`;

const WORDS = [
  {
    word: 'Fowl',
    fontFamily: fontFamilies.Typography
  },
  {
    word: 'Wowel',
    fontFamily: fontFamilies.Iconography
  },
  {
    word: 'Wowel',
    fontFamily: fontFamilies.Cryptography
  },
  {
    word: 'Wafer',
    fontFamily: fontFamilies.Typography
  },
  {
    word: 'Waver',
    fontFamily: fontFamilies.Iconography
  },
  {
    word: 'Waver',
    fontFamily: fontFamilies.Cryptography
  },
  {
    word: 'Cap',
    fontFamily: fontFamilies.Typography
  },
  {
    word: 'Gap',
    fontFamily: fontFamilies.Iconography
  },
  {
    word: 'Gap',
    fontFamily: fontFamilies.Cryptography
  },
  {
    word: 'Class',
    fontFamily: fontFamilies.Typography
  },
  {
    word: 'Glass',
    fontFamily: fontFamilies.Iconography
  },
  {
    word: 'Glass',
    fontFamily: fontFamilies.Cryptography
  },
  {
    word: 'Pear',
    fontFamily: fontFamilies.Typography
  },
  {
    word: 'Bear',
    fontFamily: fontFamilies.Iconography
  },
  {
    word: 'Bear',
    fontFamily: fontFamilies.Cryptography
  },
  {
    word: 'Plush',
    fontFamily: fontFamilies.Typography
  },
  {
    word: 'Blush',
    fontFamily: fontFamilies.Iconography
  },
  {
    word: 'Blush',
    fontFamily: fontFamilies.Cryptography
  },
  {
    word: 'Dose',
    fontFamily: fontFamilies.Typography
  },
  {
    word: 'Doze',
    fontFamily: fontFamilies.Iconography
  },
  {
    word: 'Doze',
    fontFamily: fontFamilies.Cryptography
  },
  {
    word: 'Muscle',
    fontFamily: fontFamilies.Typography
  },
  {
    word: 'Muzzle',
    fontFamily: fontFamilies.Iconography
  },
  {
    word: 'Muzzle',
    fontFamily: fontFamilies.Cryptography
  },
  {
    word: 'Tear',
    fontFamily: fontFamilies.Typography
  },
  {
    word: 'Dear',
    fontFamily: fontFamilies.Iconography
  },
  {
    word: 'Dear',
    fontFamily: fontFamilies.Cryptography
  },
  {
    word: 'Try',
    fontFamily: fontFamilies.Typography
  },
  {
    word: 'Dry',
    fontFamily: fontFamilies.Iconography
  },
  {
    word: 'Dry',
    fontFamily: fontFamilies.Cryptography
  },
  {
    word: 'Cheer',
    fontFamily: fontFamilies.Typography
  },
  {
    word: 'Jeer',
    fontFamily: fontFamilies.Iconography
  },
  {
    word: 'Jeer',
    fontFamily: fontFamilies.Cryptography
  },
  {
    word: 'Chump',
    fontFamily: fontFamilies.Typography
  },
  {
    word: 'Jump',
    fontFamily: fontFamilies.Iconography
  },
  {
    word: 'Jump',
    fontFamily: fontFamilies.Cryptography
  }
];

export default SecondSection;
